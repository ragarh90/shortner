## Shortly service

**Description:** Small service to create shortcodes for longs URLs

## Running Instructions

1. Install docker & docker-compose in your computer, you can find instrucctions to intall docker [here](https://docs.docker.com/desktop/) and for docker-compose [here](https://docs.docker.com/compose/install/)

2. After installing both you need to define some `.env` files nside the `configs` folder
    - `db.env`: This file contains the root users and the initial database for your mongoDB container

    ```
    MONGO_INITDB_DATABASE=shortly_db
    MONGO_INITDB_ROOT_USERNAME=<USER>
    MONGO_INITDB_ROOT_PASSWORD=<PASSWORD>  
    ```

    - `app.env`: This file should contain the database connections for mongoDB storage used by sinatra app, make user `<USER>` and `<PASSWORD>` matches the ones defined in `db.env`

    ```
    PROD_MONGODB_URI=mongodb://<USER>:<PASSWORD>@db:27017/<PRODUCTION_DATABASE>  
    DEV_MONGODB_URI=mongodb://<USER>:<PASSWORD>@db:27017/<DEVELOPMENT_DATABASE>
    TEST_MONGODB_URI=mongodb://<USER>:<PASSWORD>@db:27017/<TEST_DATABASE>
    ```

3.  Then you need to run the command `docker-compose up` and it will launch all the required components to run the application
4. To ensure is running go to http://localhost:4567 and you should see a message like `Hello world, this is shortly app at 2021-02-15 05:44:31 +0000`


## Run rspec test
1. Access to the container bash with `sudo docker-compose run -e SHORTLY_ENV=test web bash`
2. Inside the bash cli type `rspec spec/*` to run all tests