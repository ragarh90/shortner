require './configs/enviroment'
require './models/shortner'
ENV['RACK_ENV'] = @env
RSpec.configure do |config|
  config.include Rack::Test::Methods
  config.before(:suite) do
    connection_config = {
      "production" => { 'uri' =>  ENV['PROD_MONGODB_URI'] },
      "development" => {'uri' =>  ENV['DEV_MONGODB_URI'] },
      "test" => { 'uri' =>  ENV['TEST_MONGODB_URI'] }
    }
    @env = ENV["SHORTLY_ENV"] || "test"
    MongoMapper.setup(connection_config, @env)
  end
  config.after(:each) do
    MongoMapper.database.collections.each(&:drop)
  end

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.filter_run :focus
  config.run_all_when_everything_filtered = true
  config.disable_monkey_patching!
  if config.files_to_run.one?
    config.default_formatter = 'doc'
  end 
  config.profile_examples = 10
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    
    with.library :active_model
  end
end
